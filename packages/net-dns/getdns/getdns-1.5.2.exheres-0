# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A modern asynchronous DNS API"
HOMEPAGE="https://getdnsapi.net"
DOWNLOADS="${HOMEPAGE}/dist/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    libev [[ description = [ Use libev for running loops ] ]]
    libevent [[ description = [ Use libevent for waiting on socket events ] ]]
    stub-only [[ description = [ Build for stub resolution mode only ] ]]
    ( providers: gnutls openssl ) [[
        number-selected = at-least-one
        *description = [ Provide TLS support ]
    ]]
    ( providers: libressl openssl ) [[
        number-selected = exactly-one
        *description = [ Provide libcrypto support ]
    ]]
"

# Require net access
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libbsd
        dev-libs/libuv
        net-dns/libidn2:=
        libev? ( dev-libs/libev )
        libevent? ( dev-libs/libevent:= )
        !stub-only? ( net-dns/unbound )
        providers:gnutls? (
            dev-libs/gnutls[dane]
            dev-libs/nettle
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=1.0.2] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-dsa
    --enable-ecdsa
    --enable-gost
    --disable-static
    --with-libuv
    --with-piddir=/run
    --with-ssl=/usr/$(exhost --target)
)

# NOTE(danyspin97) as of version 1.5.2:
# --without-gnutls execute the same action as --with-gnutls,
# CONFIGURE_OPTIONS is used to avoid this bug
DEFAULT_SRC_CONFIGURE_OPTIONS=(
    'providers:gnutls --with-gnutls'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    stub-only
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    libev
    libevent
)

